# Simple Jetty Buildr Project #

This is nice for non-java files, but still requires a restart for code changes.  So far the only option
I've found is [JRebel](http://zeroturnaround.com/jrebel/).

[This Stack Overflow](http://stackoverflow.com/questions/5226431/play-java-web-framework-how-does-their-development-server-compile-automaticall) question has some really good info.
