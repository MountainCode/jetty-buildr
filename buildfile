require 'buildr/jetty'

# Version number for this release
VERSION_NUMBER = "1.0.0"
# Group identifier for your projects
GROUP = "jetty-buildr"
COPYRIGHT = ""

# Specify Maven 2.0 remote repositories here, like this:
repositories.remote << "http://repo1.maven.org/maven2"

desc "The Jetty-buildr project"
define "jetty-buildr" do

  project.version = VERSION_NUMBER
  project.group = GROUP
  manifest["Implementation-Vendor"] = COPYRIGHT
  compile.with transitive('org.mortbay.jetty:jetty:jar:6.1.26')
  deps = compile.dependencies.collect { |t| t.to_s }
  package(:war)

  task run: :compile do
    Java::Commands.java('MyServer', classpath: compile.dependencies + [compile.target.to_s])
  end
end
