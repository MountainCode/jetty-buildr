import org.mortbay.jetty.*;
import org.mortbay.jetty.webapp.WebAppContext;

public class MyServer {
  public static void main(String[] args) {
    final Server server = new Server(8080);
    WebAppContext webAppContext = new WebAppContext("src/main/webapp", "/");
    webAppContext.setConfigurationClasses(new String[] {
      "org.mortbay.jetty.webapp.WebInfConfiguration", 
      "org.mortbay.jetty.webapp.WebXmlConfiguration"
    });
    server.addHandler(webAppContext);
    try {
      server.start();
      server.join();
    } catch(Exception ex) {
      throw new RuntimeException(ex);
    }
  }
}
